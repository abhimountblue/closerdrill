function limitFunctionCallCount(cb, n) {
  if(typeof cb !=='function'|| typeof n !='number'){
    throw new Error('partial parameters, and undefined argument is passed')
  }
  let counter = 0
  function execute(...argument) {
    if (counter < n) {
      counter++
      return cb(...argument)
    }
    return null
  }
  return execute
}

module.exports = limitFunctionCallCount
