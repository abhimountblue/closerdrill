const cacheFunction = require("../cacheFunction.cjs")
function sum(...name) {
  let sum = 0
  for (let index = 0; index < name.length; index++) {
    sum += name[index]
  }
  return sum
}
try {
  const result = cacheFunction(sum)
  console.log(result(4, 5, 6, 22, 2))
  console.log(result())
} catch (error) {
  console.log(error)
}
