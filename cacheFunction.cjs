function cacheFunction(cb) {
  if (typeof cb !== "function") {
    throw new Error(" partial parameters, and undefined argument is passed ")
  }
  const cachedObject = {}
  function execute(...argument) {
    let argumentToString = JSON.stringify(argument)
    if (cachedObject.hasOwnProperty(argumentToString)) {
      return cachedObject[argumentToString]
    } else {
      const result = cb(...argument)
      cachedObject[argumentToString] = result
      return result
    }
  }
  return execute
}

module.exports = cacheFunction
